<head>
    <title>Raport Anak</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>

<div class="div-logo">
    <img src="{{ asset('images/logo-raport.png') }}" width="100%">
</div>
<div class="div-judul">
    <h3 align="center">PEMERINTAH KABUPATEN BADUNG</h3>
    <h3 align="center">DINAS PENDIDIKAN KEBUDAYAAN PEMUDA DAN OLAH RAGA </h3>
    <h2 align="center">SDN 4 UNGASAN</h2>
    <h2 align="center">KECAMATAN KUTA SELATAN KABUPATEN BADUNG</h2>
</div>
<div class="clearfix"></div>
<hr/>
<h3 align="center">LAPORAN PENILAIAN HASIL BELAJAR SISWA</h3>
<h3 align="center">TAHUN PELAJARAN {{ (date('Y')-1).'/'.(date('Y')-0) }}</h3>
<br/>
<table class="table table-bordered">
    <tr>
        <td width="20" align="center">No</td>
        <td>Nama Lengkap Siswa</td>
        <td align="center">NISN</td>
        <td align="center">Kelas</td>
    </tr>
    <tr>
        <td align="center">1</td>
        <td>{{ $siswa->swa_nama }}</td>
        <td align="center">{{ $siswa->swa_nis }}</td>
        <td align="center">{{ $siswa->kelas->kls_nama }}</td>
    </tr>
</table>
<br/>
<table class="table table-bordered">
    <tr>
        <td rowspan="2" width="20" align="center">No</td>
        <td rowspan="2" align="center">Mata Pelajaran</td>
        <td rowspan="2" align="center">KKM</td>
        <td colspan="2" align="center">Nilai</td>
        <td rowspan="2" align="center">Deskripsi Kemajuan Belajar</td>
    </tr>
    <tr>
        <td align="center">Angka</td>
        <td align="center">Huruf</td>
    </tr>
    @php
        $total = 0;
        $jumlah = 0;
    @endphp
    @foreach($nilai as $key => $row)
        @php
            $total += $row->nilai;
            $jumlah++;
        @endphp
    <tr>
        <td align="center">{{ ++$key }}</td>
        <td>{{ $row->mata_pelajaran->mpj_nama }}</td>
        <td align="center">{{ $row->mata_pelajaran->mpj_nilai_lulus }}</td>
        <td align="center">{{ $row->nilai }}</td>
        <td>{{ Main::terbilang($row->nilai) }}</td>
        <td>{{ $row->nilai >= $row->mata_pelajaran->mpj_nilai_lulus ? 'KKM Tercapai':'KKM Tidak Tercapai' }}</td>
    </tr>
    @endforeach
    <tr>
        <td></td>
        <td colspan="2">Jumlah</td>
        <td align="center">{{ $total }}</td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">Rata-Rata</td>
        <td align="center">{{ round($total/$jumlah) }}</td>
        <td colspan="2"></td>
    </tr>
</table>
<br/>
<br/>
<table width="100%">
    <tr>
        <td width="20"></td>
        <td width="20%">Mengetahui</td>
        <td width="50%"></td>
        <td width="30%">Jimbaran, {{ date('d F Y') }}</td>
    </tr>
    <tr>
        <td></td>
        <td>Orang Tua/Wali</td>
        <td></td>
        <td>Wali Kelas</td>
    </tr>
    <tr>
        <td height="50"></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><strong>{{ $orang_tua->ort_nama_ayah ? $orang_tua->ort_nama_ayah : $orang_tua->ort_nama_ibu }}</strong></td>
        <td></td>
        <td><strong>{{ $guru_wali->gru_nama }}</strong></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>NIP. {{ $guru_wali->gru_nip }}</td>
    </tr>
</table>

</body>



