<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);


Route::group(['namespace' => 'MataPelajaran'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/matapelajaran', 'MataPelajaran@index')->name('matapelajaranList');
        Route::post('/matapelajaran/datatable', 'MataPelajaran@data_table')->name('matapelajaranDataTable');
        Route::get('/matapelajaran/create', 'MataPelajaran@create')->name('matapelajaranCreate');
        Route::post('/matapelajaran/insert', 'MataPelajaran@insert')->name('matapelajaranInsert');
        Route::delete('/matapelajaran/delete/{id}', "MataPelajaran@delete")->name('matapelajaranDelete');
        Route::get('/matapelajaran/edit/{id}', 'MataPelajaran@edit_modal')->name('matapelajaranEditModal');
        Route::post('/matapelajaran/update/{id}', 'MataPelajaran@update')->name('matapelajaranUpdate');

    });

});

Route::group(['namespace' => 'Jadwal'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/jadwal', "Jadwal@index")->name('jadwalList');
        Route::post('/jadwal/update', "Jadwal@update")->name('jadwalUpdate');

    });

});

Route::group(['namespace' => 'Guru'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/guru', "Guru@index")->name('guruList');
        Route::post('/guru/datatable', 'Guru@data_table')->name('guruDataTable');
        Route::get('/guru/create', 'Guru@create')->name('guruCreate');
        Route::post('/guru/insert', "Guru@insert")->name('guruInsert');
        Route::delete('/guru/delete/{id}', "Guru@delete")->name('guruDelete');
        Route::get('/guru/edit/{id}', "Guru@edit_modal")->name('guruEditModal');
        Route::post('/guru/update/{id}', "Guru@update")->name('guruUpdate');

    });

});

Route::group(['namespace' => 'Kelas'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/kelas', "Kelas@index")->name('kelasList');
        Route::post('/kelas/datatable', 'Kelas@data_table')->name('kelasDataTable');
        Route::get('/kelas/create', 'Kelas@create')->name('kelasCreate');
        Route::post('/kelas/insert', "Kelas@insert")->name('kelasInsert');
        Route::delete('/kelas/delete{id}', "Kelas@delete")->name('kelasDelete');
        Route::get('/kelas/edit/{id}', "Kelas@edit_modal")->name('kelasEditModal');
        Route::post('/kelas/update/{id}', "Kelas@update")->name('kelasUpdate');

    });

});

Route::group(['namespace' => 'Siswa'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/siswa', "Siswa@index")->name('siswaList');
        Route::post('/siswa/datatable', 'Siswa@data_table')->name('siswaDataTable');
        Route::get('/siswa/create', 'Siswa@create')->name('siswaCreate');
        Route::post('/siswa/insert', "Siswa@insert")->name('siswaInsert');
        Route::delete('/siswa/{id}', "Siswa@delete")->name('siswaDelete');
        Route::get('/siswa/{id}', "Siswa@edit_modal")->name('siswaEditModal');
        Route::post('/siswa/{id}', "Siswa@update")->name('siswaUpdate');

    });

});

Route::group(['namespace' => 'OrangTua'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/orangtua', "OrangTua@index")->name('orangtuaList');
        Route::post('/orangtua/datatable', 'OrangTua@data_table')->name('orangtuaDataTable');
        Route::get('/orangtua/create', 'OrangTua@create')->name('orangtuaCreate');
        Route::post('/orangtua/insert', "OrangTua@insert")->name('orangtuaInsert');
        Route::delete('/orangtua/{id}', "OrangTua@delete")->name('orangtuaDelete');
        Route::get('/orangtua/{id}', "OrangTua@edit_modal")->name('orangtuaEditModal');
        Route::post('/orangtua/{id}', "OrangTua@update")->name('orangtuaUpdate');

    });

});

Route::group(['namespace' => 'Nilai'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/nilai', "Nilai@index")->name('nilaiList');
        Route::post('/nilai/datatable', 'Nilai@data_table')->name('nilaiDataTable');
        Route::get('/nilai/create', 'Nilai@create')->name('nilaiCreate');
        Route::post('/nilai/insert', "Nilai@insert")->name('nilaiInsert');
        Route::delete('/nilai/{id}', "Nilai@delete")->name('nilaiDelete');
        Route::get('/nilai/{id}', "Nilai@edit_modal")->name('nilaiEditModal');
        Route::post('/nilai/{id}', "Nilai@update")->name('nilaiUpdate');

    });

});

Route::group(['namespace' => 'Absen'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/absen', "Absen@index")->name('absenList');
        Route::post('/absen/datatable', 'Absen@data_table')->name('absenDataTable');
        Route::get('/absen/create', 'Absen@create')->name('absenCreate');
        Route::post('/absen/insert', "Absen@insert")->name('absenInsert');
        Route::delete('/absen/{id}', "Absen@delete")->name('absenDelete');
        Route::get('/absen/{id}', "Absen@edit_modal")->name('absenEditModal');
        Route::post('/absen/{id}', "Absen@update")->name('absenUpdate');

    });

});

Route::group(['namespace' => 'LaporanSiswa'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/laporansiswa', "LaporanSiswa@index")->name('laporansiswaList');
        Route::post('/laporansiswa/datatable', 'LaporanSiswa@data_table')->name('laporansiswaDataTable');
        Route::get('/laporansiswa/create', 'LaporanSiswa@create')->name('laporansiswaCreate');
        Route::post('/laporansiswa/insert', "LaporanSiswa@insert")->name('laporansiswaInsert');
        Route::delete('/laporansiswa/{id}', "LaporanSiswa@delete")->name('laporansiswaDelete');
        Route::get('/laporansiswa/{id}', "LaporanSiswa@edit_modal")->name('laporansiswaEditModal');
        Route::post('/laporansiswa/{id}', "LaporanSiswa@update")->name('laporansiswaUpdate');

    });

});

Route::group(['namespace' => 'LaporanAnak'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/laporananak', "LaporanAnak@index")->name('laporananakList');
        Route::post('/laporananak/datatable', 'LaporanAnak@data_table')->name('laporananakDataTable');
        Route::get('/laporananak/create', 'LaporanAnak@create')->name('laporananakCreate');
        Route::post('/laporananak/insert', "LaporanAnak@insert")->name('laporananakInsert');
        Route::delete('/laporananak/{id}', "LaporanAnak@delete")->name('laporananakDelete');
        Route::get('/laporananak/{id}', "LaporanAnak@edit_modal")->name('laporananakEditModal');
        Route::post('/laporananak/{id}', "LaporanAnak@update")->name('laporananakUpdate');

    });

});

Route::namespace('General')->group(function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
    Route::post('city/list', "General@city_list")->name('cityList');
    Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/dashboard/admin', "DashboardAdmin@index")->name('dashboardadminPage');
        Route::get('/whatsapp-test', "DashboardAdmin@whatsapp_test")->name('whatsappTest');

        Route::get('/dashboard/guru', "DashboardGuru@index")->name('dashboardguruPage');
        Route::get('/whatsapp-test', "DashboardGuru@whatsapp_test")->name('whatsappTest');

        Route::get('/dashboard/kepala-sekolah', "DashboardKepalaSekolah@index")->name('dashboardkepsekPage');
        Route::get('/whatsapp-test', "DashboardKepalaSekolah@whatsapp_test")->name('whatsappTest');

        Route::get('/dashboard/orang-tua', "DashboardOrangTua@index")->name('dashboardortuPage');
        Route::get('/whatsapp-test', "DashboardOrangTua@whatsapp_test")->name('whatsappTest');

        Route::get('/statistikabsensi', "StatistikAbsensi@index")->name('statistikabsensiPage');
        Route::get('/statistikabsensi/anak', "StatistikAbsensiAnak@index")->name('statistikabsensianakPage');

        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');

        Route::post('/cetak/pdf', "General@cetak_pdf")->name('cetakPdf');

    });
});

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin'], function () {

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

    // Metode Tindakan
    Route::get('/metode-tindakan', "ActionMethod@index")->name('actionMethodPage');
    Route::post('/metode-tindakan', "ActionMethod@insert")->name('actionMethodInsert');
    Route::get('/metode-tindakan-list', "ActionMethod@list")->name('actionMethodList');
    Route::delete('/metode-tindakan/{kode}', "ActionMethod@delete")->name('actionMethodDelete');
    Route::get('/metode-tindakan/modal/{kode}', "ActionMethod@edit_modal")->name('actionMethodEditModal');
    Route::post('/metode-tindakan/{kode}', "ActionMethod@update")->name('actionMethodUpdate');

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

